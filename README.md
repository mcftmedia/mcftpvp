# McftPvP

A Bukkit server plugin for Minecraft that allows you to have permissions for both groups and users for PvP

This was made to suit the needs of the [Diamond Mine](http://diamondmine.net) server.

**[License](https://diamondmine.net/plugins/license)** -- **[Issues](https://mcftmedia.atlassian.net/issues/?jql=project=PVP)** --  **[Builds](https://mcftmedia.atlassian.net/builds/browse/PVP-PVP)**
-- **[Download](http://diamondmine.net/plugins/download/McftPvP)** -- **[Bukkit Dev](http://dev.bukkit.org/server-mods/mcftpvp/)**