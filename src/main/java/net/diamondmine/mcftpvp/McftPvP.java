package net.diamondmine.mcftpvp;

import java.util.logging.Logger;

import net.diamondmine.mcftpvp.listeners.EntityListener;
import net.milkbowl.vault.permission.Permission;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Core of McftPvP.
 * 
 * @author Jon la Cour
 * @version 1.0.3
 */
public class McftPvP extends JavaPlugin {
    private final EntityListener entityListener = new EntityListener(this);
    public static final Logger logger = Logger.getLogger("Minecraft");
    private FileConfiguration config;
    public static Permission p;

    /**
     * Plugin disabled.
     */
    @Override
    public final void onDisable() {
        saveConfig();
        log("Version " + getDescription().getVersion() + " is disabled!");
    }

    /**
     * Plugin enabled.
     */
    @Override
    public final void onEnable() {
        // Permissions
        if (!setupPermissions()) {
            getServer().getPluginManager().disablePlugin(this);
            return;
        }

        // Settings
        config = getConfig();
        checkConfig();

        PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(entityListener, this);
    }

    /**
     * Checks to make sure a permissions plugin has been loaded.
     * 
     * @return True if one is present, otherwise false.
     * @since 1.0.0
     */
    private boolean setupPermissions() {
        RegisteredServiceProvider<Permission> permissionProvider = getServer().getServicesManager().getRegistration(
                net.milkbowl.vault.permission.Permission.class);
        if (permissionProvider != null) {
            p = permissionProvider.getProvider();
        }
        return (p != null);
    }

    /**
     * Sets default configuration up if there isn't a configuration file
     * present.
     * 
     * @since 1.0.2
     */
    private void checkConfig() {
        config.options().copyDefaults(true);
        config.addDefault("deny-attack", "- You are not allowed to attack other players.");
        config.addDefault("deny-protected", "- You can not attack %d, his group is protected.");
        config.addDefault("deny-own-group", "- You are not allowed to attack members of your own group.");
        config.options().copyDefaults(true);
        saveConfig();
    }

    /**
     * Gets the configuration.
     * 
     * @return FileConfiguration
     * @since 1.0.0
     */
    public final FileConfiguration getConfiguration() {
        return config;
    }

    /**
     * Sends a message to the logger.
     * 
     * @param s
     *            The message to send
     * @param type
     *            The level (info, warning, severe)
     * @since 1.0.0
     */
    public static void log(final String s, final String type) {
        String message = "[McftPvP] " + s;
        String t = type.toLowerCase();
        if (t != null) {
            boolean info = t.equals("info");
            boolean warning = t.equals("warning");
            boolean severe = t.equals("severe");
            if (info) {
                logger.info(message);
            } else if (warning) {
                logger.warning(message);
            } else if (severe) {
                logger.severe(message);
            } else {
                logger.info(message);
            }
        }
    }

    /**
     * Sends a message to the logger.
     * 
     * @param s
     *            The message to send.
     */
    public static void log(final String s) {
        log(s, "info");
    }
}
