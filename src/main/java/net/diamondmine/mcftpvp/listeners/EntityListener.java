package net.diamondmine.mcftpvp.listeners;

import net.diamondmine.mcftpvp.McftPvP;

import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

/**
 * Event handlers.
 * 
 * @author Jon la Cour
 * @version 1.0.3
 */
public class EntityListener implements Listener {
    private final McftPvP plugin;

    /**
     * Constructor.
     * 
     * @param instance
     *            McftPvP class.
     */
    public EntityListener(final McftPvP instance) {
        plugin = instance;
    }

    /**
     * Checks who damaged who.
     * 
     * @param event
     *            EntityDamageEvent event.
     * @since 1.0.0
     */
    @EventHandler(priority = EventPriority.NORMAL)
    public final void onEntityDamage(final EntityDamageEvent event) {
        if (event instanceof EntityDamageByEntityEvent) {
            EntityDamageByEntityEvent entEvent = (EntityDamageByEntityEvent) event;
            if ((entEvent.getDamager() instanceof Player) && (entEvent.getEntity() instanceof Player)) {
                Player attacker = (Player) entEvent.getDamager();
                Player defender = (Player) entEvent.getEntity();
                checkPermissions(attacker, defender, entEvent);
            } else if (entEvent.getDamager() instanceof Projectile) {
                Projectile projectile = (Projectile) entEvent.getDamager();
                Entity entDefender = entEvent.getEntity();
                LivingEntity entAttacker = projectile.getShooter();
                if (entAttacker instanceof Player) {
                    Player attacker = (Player) entAttacker;
                    if (entDefender instanceof Player) {
                        Player defender = (Player) entDefender;
                        checkPermissions(attacker, defender, (EntityDamageByEntityEvent) event);
                    }
                }
            }
        }
    }

    /**
     * Checks if a player has permission to attack another player.
     * 
     * @param attacker
     *            The attacker player object.
     * @param defender
     *            The defender player object.
     * @param event
     *            The EntityDamageByEntityEvent event.
     * @since 1.0.3
     */
    private void checkPermissions(final Player attacker, final Player defender, final EntityDamageByEntityEvent event) {
        String world = attacker.getWorld().toString();
        world = world.replace("CraftWorld{name=", "");
        world = world.replace("}", "");
        if (McftPvP.p.has(attacker, "mcftpvp.denyattack")) {
            event.setCancelled(true);
            String notification = null;
            String defendingGroup = McftPvP.p.getPrimaryGroup(world, defender.getName());
            notification = plugin.getConfiguration().getString("deny-attack").replace("%d", defender.getName());
            notification = notification.replace("%g", defendingGroup);
            attacker.sendMessage(notification);
        } else {
            if (McftPvP.p.has(defender, "mcftpvp.protected")) {
                event.setCancelled(true);
                String notification = null;
                String defendingGroup = McftPvP.p.getPrimaryGroup(world, defender.getName());
                notification = plugin.getConfiguration().getString("deny-protected").replace("%d", defender.getName());
                notification = notification.replace("%g", defendingGroup);
                attacker.sendMessage(notification);
            } else {
                String attackingGroup = McftPvP.p.getPrimaryGroup(world, attacker.getName());
                String defendingGroup = McftPvP.p.getPrimaryGroup(world, defender.getName());
                if (McftPvP.p.has(attacker, "mcftpvp.blockgroup") && attackingGroup.equals(defendingGroup)) {
                    event.setCancelled(true);
                    String notification = null;
                    notification = plugin.getConfiguration().getString("deny-own-group").replace("%d", defender.getName());
                    notification = notification.replace("%g", attackingGroup.trim());
                    attacker.sendMessage(notification);
                }
            }
        }
    }
}
